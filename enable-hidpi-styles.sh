xfconf-query -c xsettings -p /Xfce/LastCustomDPI -s 192
xfconf-query -c xsettings -p /Xft/DPI -s 192
xfconf-query -c xsettings -p /Gtk/IconSizes -s "gtk-large-toolbar=96,96:gtk-small-toolbar=64,64:gtk-menu=64,64:gtk-dialog=96,96:gtk-button=64,64:gtk-dnd=64,64"
xfconf-query -c xsettings -p /Gtk/CursorThemeSize -s 48
xfconf-query -c xsettings -p /Gtk/FontName -s "Ubuntu 10"
xfconf-query -c xfwm4 -p /general/theme -s "Numix-hdpi"
xfconf-query -c xfwm4 -p /general/title_font -s "Ubuntu Bold 10"
xfconf-query -c xfce4-panel -p /panels/panel-1/size -s 48
xfconf-query -c xfce4-panel -p /plugins/plugin-3/size-max -s 48
source ./get_default_gnome_terminal_profile.sh
dconf write ${gnome_legacy_profiles_path}${default_gnome_terminal_profile_hash}/font "'Hack 14'"
sed -i 's/FontName.*/FontName=Hack 14/g' ~/.config/xfce4/terminal/terminalrc
