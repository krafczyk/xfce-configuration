xfconf-query -c xsettings -p /Xfce/LastCustomDPI -s 96
xfconf-query -c xsettings -p /Xft/DPI -s 96
xfconf-query -c xsettings -p /Gtk/IconSizes -s ""
xfconf-query -c xsettings -p /Gtk/CursorThemeSize -s 24
xfconf-query -c xsettings -p /Gtk/FontName -s "Ubuntu 13"
xfconf-query -c xfwm4 -p /general/theme -s "Numix"
xfconf-query -c xfwm4 -p /general/title_font -s "Ubuntu Bold 13"
xfconf-query -c xfce4-panel -p /panels/panel-1/size -s 30
xfconf-query -c xfce4-panel -p /plugins/plugin-3/size-max -s 30
source ./get_default_gnome_terminal_profile.sh
dconf write ${gnome_legacy_profiles_path}${default_gnome_terminal_profile_hash}/font "'Hack 17'"
sed -i 's/FontName.*/FontName=Hack 17/g' ~/.config/xfce4/terminal/terminalrc
