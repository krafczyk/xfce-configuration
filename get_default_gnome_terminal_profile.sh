export default_gnome_terminal_profile_hash=""
export gnome_legacy_profiles_path="/org/gnome/terminal/legacy/profiles:/"

for hash in $(dconf dump ${gnome_legacy_profiles_path} | grep "\[:" | sed 's/\[//' | sed 's/\]//'); do
	if [ "$(dconf read ${gnome_legacy_profiles_path}${hash}/visible-name)" = "'Default'" ]; then
		export default_gnome_terminal_profile_hash=${hash}
		break
	fi;
done;
